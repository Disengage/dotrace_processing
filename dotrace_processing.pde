//Dot test;
Population test;

PVector goal;

void setup() {
  frameRate(120);
  size(800, 600);
  goal = new PVector(width / 2, 80);
  
  //test = new Dot();
  test = new Population(100);
}

void draw() {
  background(255);
  
  fill(255, 0, 0); // Add goal
  ellipse(goal.x, goal.y, 20, 20);
  
  fill(140, 140, 140); // Add obstacle
  rect(width / 4, height / 2, width / 2, 20); 
  
  if (test.allDotsIsDead()) {
    // Genetic algorithm
    test.fitnessCalculation();
    test.makeNutualSelection();
    test.makeMutateDots();
  } else {
    test.update();
    test.show();
  }
}

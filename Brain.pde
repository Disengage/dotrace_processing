class Brain {
  PVector[] directions;
  int step = 0;
  
  Brain(int size) {
    directions = new PVector[size];
    randomize();
  }
  
  void randomize() {
    for (int i = 0; i < directions.length; i++) {
      directions[i] = PVector.fromAngle(randomAngle());
    }
  }
  
  float randomAngle() {
    return random(2 * PI);
  }
  
  Brain clone() {
    Brain clone = new Brain(directions.length);
    for (int i = 0; i < directions.length; i++) {
      clone.directions[i] = directions[i].copy();
    }
    return clone;
  }
  
  void mutation() {
    float mutationRate = 0.02; // อัตราการกลายพันธ์ุ 2%
    for (int i = 0; i < directions.length; i++) {
      float randomRate = random(1); // 0 - 100
      if (randomRate < mutationRate) {
        // Set this direction as random
        directions[i] = PVector.fromAngle(randomAngle());
      }
    }
  }
}

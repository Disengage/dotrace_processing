class Population {
  Dot[] dots;
  
  float fitnessSummary;
  
  int generation = 1;
  int bestDotIndex = 0;
  
  int minStep = 400;
  
  Population(int size) {
    dots = new Dot[size];
    for(int i = 0; i < dots.length; i++) {
      dots[i] = new Dot();
    }
  }
  
  void show() {
    for(int i = 0; i < dots.length; i++) {
      dots[i].show();
    }
  }
  
  void update() {
    for(int i = 0; i < dots.length; i++) {
      dots[i].update();
    }
  }
  
  void fitnessCalculation() {
    for(int i = 0; i < dots.length; i++) {
      dots[i].fitnessCalculation();
    }
  }
  
  boolean allDotsIsDead() {
    for(int i = 0; i < dots.length; i++) {
      if (!dots[i].isDead && !dots[i].isReachedGoal) {
        return false;
      }
    }
    return true;
  }
  
  void makeNutualSelection() {
    Dot[] newPopulation = new Dot[dots.length];
    setBestDot(); // ปกติต้องหาประชากรดีสุดเพื่อช่วย generation ถัดไป
    fitnessSummaryCalculation();
    
    // Always clone the first of fitness score
    newPopulation[0] = dots[bestDotIndex].clone();
    newPopulation[0].isTheBestDot = true;
    
    // Test by clone best fitness to 10% of population
    for(int i = 1; i < 10; i++) { // ทดลอง copy ประชากรดีสุดมา 10 ตัวเลย จะได้ run เร็วๆ
      newPopulation[i] = dots[bestDotIndex].clone();
    }
    
    // Otherwise random from the existing
    for(int i = 10; i < newPopulation.length; i++) { // Skip the best finess at first item
      // Select the parent base on fitness
      Dot parent = selectedDotParent();
       
      // Get baby from them
      newPopulation[i] = parent.clone();
    }
    
    dots = newPopulation.clone(); // Replace existing population
    generation++;
    
  }
  
  void fitnessSummaryCalculation() {
    fitnessSummary = 0;
    for(int i = 0; i < dots.length; i++) {
      fitnessSummary += dots[i].fitness;
    }
  }
  
  Dot selectedDotParent() {
    float rand = random(fitnessSummary); // สุมในช่วง sum ของ fitness ทั้งหมด
    float runningSum = 0;
    for(int i = 0; i < dots.length; i++) {
      runningSum += dots[i].fitness;
      if (runningSum > rand) {
        return dots[i];
      }
    }
    return null;
  }
  
  void makeMutateDots() {
    for(int i = 10; i < dots.length; i++) { // Skip the best finess at first item 10%
      dots[i].brain.mutation();
    }
  }
  
  void setBestDot() {
    float max = 0;
    int maxIndex = 0;
    int deadSummary = 0;
    int goalSummary = 0;
    for(int i = 0; i < dots.length; i++) {
      if (dots[i].fitness > max) {
        max = dots[i].fitness;
        maxIndex = i;
      }
      if (dots[i].isDead) {
        deadSummary++;
      }
      if (dots[i].isReachedGoal) {
        goalSummary++;
      }
    }
    bestDotIndex = maxIndex;
    
    if (dots[bestDotIndex].isReachedGoal) {
      minStep = dots[bestDotIndex].brain.step; // ตัวชี้วัด
    }
    
    println("Generation: ", generation);
    print("Best step = ", minStep);
    print(", Dead = ", deadSummary);
    print(", Reach goal = ", goalSummary);
    println("\r\n");
  }
}

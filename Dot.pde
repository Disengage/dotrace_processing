class Dot {
  PVector position;
  PVector velocity;
  PVector accuracy;

  Brain brain;
  
  boolean isDead = false;

  float fitness;
  boolean isReachedGoal = false;
  boolean isTheBestDot = false;

  Dot() {
    brain = new Brain(500);
    position = new PVector(width / 2, height - 8);
    velocity = new PVector(0, 0);
    accuracy = new PVector(0, 0);
  }
  
  void show() {
    if(isTheBestDot) {
      fill(0, 0, 255);
      if (isReachedGoal) {
        fill(255, 170, 0);
      }
    } else {
      if (isDead) {
        fill(255, 0, 0);
      } else {
        fill(225, 255, 0);
      }
    }
    ellipse(position.x, position.y, 8, 8);
  }
  
  void move() {
    if (brain.directions.length > brain.step) {
      accuracy = brain.directions[brain.step];
      brain.step++;
    }
    velocity.add(accuracy); // ในตัวอย่างมีความเร่งอยู่
    velocity.limit(5); // limit each move
    position.add(velocity);
  }
  
  void update() {
    if (!isDead && !isReachedGoal) {
      move();
      if (position.x < 2 || position.y < 2 || position.x > width - 2 || position.y > height - 2) {
        isDead = true;
      } else if (position.x > width / 4 && position.x < width / 2 + (width / 4) && position.y > height / 2 && position.y < (height / 2) + 20) {
        isDead = true;
      } else if (dist(position.x, position.y, goal.x, goal.y) < 5) { // In center of goal
        // If reach goal
        isReachedGoal = true;
      }
    }
  }
  
  void fitnessCalculation() {
    if (isReachedGoal) {
      fitness = 1.0 / 16.0 + 10000.0 / (float)(brain.step * brain.step); // ต้องย้อนกลับไปดู
    } else {
      float distanceToGoal = dist(position.x, position.y, goal.x, goal.y);
      fitness = 1.0 / (distanceToGoal * distanceToGoal);
    }
  }
  
  Dot clone() {
    Dot clone = new Dot();
    clone.brain = brain.clone();    
    return clone;
  }
  
}
